# Die verrückten Primzahlwürfel

"Das Rätsel der Woche" in Spiegel-Online vom 6. April 2019 "[Die verrückten Primzahlwürfel](http://www.spiegel.de/wissenschaft/mensch/die-verrueckten-primzahlwuerfel-raetsel-der-woche-a-1261395.html)" lässt sich sehr schön mit Streams programmieren.

## Die Aufgabe

Vor Ihnen liegen drei sehr spezielle Würfel. Jeder der Würfel hat acht(!) Seiten, und auf jedem Würfel finden sich die Primzahlen 2, 3, 5, 7, 11, 13, 17, 19.

Die Frage: Wie groß ist die Wahrscheinlichkeit, dass die Summe der Würfelaugen 30 ergibt, wenn Sie einen Wurf mit den drei Würfeln ausführen?

## Der Code

~~~ java
List<Integer> primes = List.of(2, 3, 5, 7, 11, 13, 17, 19);

Supplier<Stream<Integer>> s = () -> primes.stream().flatMap(p1 ->
    primes.stream().flatMap(p2 ->
        primes.stream().map(p3 -> p1 + p2 + p3)));

var ratio = (double)(s.get().filter(s -> s == 30).count()) / s.get().count();

~~~

Die Lösung:

~~~
jshell> s.get().count()
$19 ==> 512

jshell> s.get().filter(sum -> sum == 30).count()
$20 ==> 6

jshell> ratio
ratio ==> 0.01171875
~~~

6 von 512 gleichwahrscheinlichen Würfelsummen ergeben den Wert 30. Die Wahrscheinlichkeit beträgt 6.0/512 = 0.01171875, also rund 1.2%.

## Hinweise zum Code

1. Da sich ein Stream mit seinem Einsatz "verbraucht", ist der Stream-Ausdruck in einem Lambda-Ausdruck "eingepackt", so dass mit einem `get` jedesmal ein neuer Stream erzeugt wird.
2. Das Casting `(double)` ist notwendig, da die Methode
`count()` einen `long`-Wert liefert. Die Division würde ohne das Casting ganzzahlig ausfallen, d.h. hier zu Null werden.

## Alternativer Code mit einem Array

Ein Array von `primes` aus `int`-Werten führt zu einer kleinen Änderung im Aufbau des Streams und ändert den Typ zu `IntStream`. 

~~~ java
int[] primes = {2, 3, 5, 7, 11, 13, 17, 19};

Supplier<IntStream> s = () -> Arrays.stream(primes).flatMap(p1 -> 
    Arrays.stream(primes).flatMap(p2 ->
        Arrays.stream(primes).map(p3 -> p1 + p2 +p3)));
~~~

Ein `IntStream` ist ein spezialisierter Strom für primitive `int`-Werte, siehe z.B. die [Doku zu Java 11](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/stream/IntStream.html)



